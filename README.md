# Personal site

<p align="center">
  <a href="https://ssd.jpl.nasa.gov/tools/sbdb_lookup.html#/?sstr=21633&view=VOP">
    <img alt="Personal site" src="./static/21633-orbit-viewer-snapshot.jpg"/>
  </a>
</p>
<h2 align="center">
  penonek.com
</h2>

[![Netlify Status](https://api.netlify.com/api/v1/badges/17e25c50-0cb7-4e6b-b256-31559bca5029/deploy-status)](https://app.netlify.com/sites/gallant-montalcini-ee7b82/deploys)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat)](https://github.com/RichardLitt/standard-readme)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/penyuan/personal-site)](https://api.reuse.software/info/gitlab.com/penyuan/personal-site)

*Source code and material for personal site*

This is the Git repository for my personal site, built using [Hugo](https://gohugo.io/) with the [Monochrome](https://github.com/kaiiiz/hugo-theme-monochrome) theme and deployed through [Netlify](https://www.netlify.com/).

## ⚙️ Install

A reasonably up-to-date [GNU/Linux](https://www.wikidata.org/wiki/Q3251801) environment with [Git](https://git-scm.com/) and familiarity with their use on the commandline is assumed. I tend to use [RHEL](https://en.wikipedia.org/wiki/Red_Hat_Enterprise_Linux)/[Fedora](https://en.wikipedia.org/wiki/Fedora_Linux)-based distributions.

1. Install [Go](https://go.dev/) and Hugo (Go is needed for [Hugo module](https://gohugo.io/hugo-modules/use-modules/) support which I use for themes), e.g.: 

   `sudo dnf install golang hugo`

2. Clone from [this Git repository](https://gitlab.com/penyuan/personal-site) to location of choice, e.g.: 

   `git clone https://gitlab.com/penyuan/personal-site.git`

3. `cd` into the cloned Git repository.

## 👩‍💻 Usage

At time of writing, the plan is for deployments to happen from the `main` branch. Development happens in the `dev` branch. When a statisfactory state is reached in the `dev` branch, it will be merged into `main`.

🎯 ***TODO:** Explain where things go, `main` vs `dev` branches, common Hugo commands, etc.*

## 🍳 Deployment

🎯 ***TODO:** How to deploy to Netlify, including branches if and when I implement that.*

## 🌱 Maintainers

@penyuan is the current sole maintainer.

## 🤗 Contributing

I am not actively seeking external contributions to this repository at this time, but please feel free to open an issue here or use the contact information on the website to get in touch with feedback.

## ❤️ License

[![CC BY-SA](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

This README and all non-code original content for the site contained in this repository are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) unless otherwise noted © 2022 Pen-Yuan Hsing

Details and information on files with other licenses are in the [REUSE](https://reuse.software/) specification [dep5](./.reuse/dep5) file.