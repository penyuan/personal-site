---
type: 'blank'
menu: navbar
navbar_title: "Pen-Yuan Hsing"
toc: false
---

I'm a researcher with highly multidisciplinary experience across citizen science, open science, and ecology & conservation. Please read on for a summary of my work in these fields, click **[here](https://contact.do/967Y)** to contact me, or see my [ORCID publication record](https://orcid.org/0000-0002-5394-879X).

----------

#### Citizen science

I am the co-founder of the [MammalWeb](https://www.MammalWeb.org/) project ([@MammalWeb on Twitter](https://www.twitter.com/MammalWeb/), for those who still use it!), where we work with citizen scientists to monitor biodiversity in their communities with motion-sensing camera traps. Since 2014, MammalWeb has grown from north-east England into a network across 5+ European countries. Citizen scientists have contributed more than 340 years of cumulative observation time and uploaded more than 2 million wildlife photos/vidoes to our web platform.

As part of the [Citizen Science Global Partnership](http://citizenscienceglobal.org/), I helped provide a citizen science perspective to the [UNESCO Recommendation for Open Science](https://en.unesco.org/science-sustainable-future/open-science/recommendation) which was ratified by United Nations members states in November 2022.

#### Open science

As scientists, we have a responsibility to the scientific process by making our work available for others to build on. As an advocate for open science, I created a short [online course on open science](https://eu-citizen.science/resource/252) for citizen science practitioners, edited a [Guide to Reproducible Code](https://www.britishecologicalsociety.org/wp-content/uploads/2019/06/BES-Guide-Reproducible-Code-2019.pdf), and was among the first to receive official [certification from the Creative Commons](https://certificates.creativecommons.org/about/certificate-graduates/) on copyright and open licensing. 

I am also privileged to be elected as a Community Councilor of the [Gathering for Open Science Hardware (GOSH)](https://openhardware.science/) and board member of the Open Science Hardware Foundation. These experiences allow me to act in an advisory capacity for [NASA](https://science.nasa.gov/open-science/transform-to-open-science) and [UNESCO](https://www.unesco.org/en/natural-sciences/open-science/implementation) in their open science initiatives.

#### Ecology & conservation

My original scientific training is in ecology and conservation. Over the past 15 years, my fieldwork took me from the rainforests of Costa Rica, the savannahs of South Africa, hydrothermal vents off the coast of Papua New Guinea, to visiting [oil-spill-impacted corals](https://news.psu.edu/story/150547/2012/03/26/research-reveals-deep-ocean-impact-deepwater-horizon-oil-spill) on the bottom of the Gulf of Mexico [in a submarine](https://divediscover.whoi.edu/archives/expedition13/interviews/divers.html).

----------

Starting the MammalWeb project combined ecological research with my background in science outreach by working with citizen scientists. I've since learned, though, citizen science is much more than just crowdsourcing data collection, and I am interested in exploring the diverse interactions between science, society, and citizenship.

And whether it's through open science or other open source communities I participate in, underlying my work is a dedication to the freedoms to use, study, remix, and share that enables the creativity and innovation that are required to tackle the huge challenges we face today.